package fr.yodamad.ai;


import dev.langchain4j.memory.ChatMemory;
import dev.langchain4j.memory.chat.MessageWindowChatMemory;
import dev.langchain4j.model.openai.OpenAiChatModel;
import dev.langchain4j.service.AiServices;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    interface Assistant {
        String chat(String message);
    }

    private static String API_KEY = "eyJhbGciOiJFZERTQSIsImtpZCI6IkVGNThFMkUxMTFBODNCREFEMDE4OUUzMzZERTk3MDhFNjRDMDA4MDEiLCJraW5kIjoib2F1dGgyIiwidHlwIjoiSldUIn0.eyJBY2Nlc3NUb2tlbiI6IjRlODc2MGY1YWJhMjhlYzhiYTE1N2QwZmMwOWNlMTRjYjc1MTMxZDk4OTFiZjRlOGI1OTU2N2ZhODJlNjdlMzMiLCJpYXQiOjE3MTQ1NDkxMjF9.5RAFXlq4nMjUyA0-JmMv9mA35BPZocHBWtqDyhx00ROatFRq3XssswIviayxdk3X0KVCO7bXseqkF7-jqkglCA";
    private static int NB_TOKENS = 1024;

    enum Models {

        Mixtral8x7b("https://mixtral-8x7b-instruct-v01.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1", "Mixtral-8x7B-Instruct-v0.1"),
        Llama213b("https://llama-2-13b-chat-hf.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1", "Llama-2-13b-chat-hf"),
        Mistral7b("https://mistral-7b-instruct-v02.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1","Mistral-7B-Instruct-v0.2"),
        CodeLLama13b("https://codellama-13b-instruct-hf.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1", "CodeLlama-13b-Instruct-hf");

        private String baseUrl;
        private String model;

        Models(String url, String modelName) {
            baseUrl = url;
            model = modelName;
        }
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Which model you want to use ?");
        for (int i = 0; i < Models.values().length; i++) {
            System.out.println((i+1) + ". " + Models.values()[i].model);
        }

        System.out.print("Pick one : ");
        Scanner in = new Scanner(System.in);
        String choice = in.nextLine();

        Models modelToUse = Models.values()[Integer.parseInt(choice)-1];
        System.out.println("Using model " + modelToUse.model + " ...");

        ChatMemory chatMemory = MessageWindowChatMemory.withMaxMessages(10);

        OpenAiChatModel model = OpenAiChatModel.builder()
                .baseUrl(modelToUse.baseUrl)
                .apiKey(API_KEY)
                .modelName(modelToUse.model)
                .maxTokens(NB_TOKENS)
                .build();

        Assistant assistant = AiServices.builder(Assistant.class)
                .chatLanguageModel(model)
                .chatMemory(chatMemory)
                .build();

        System.out.println("> Ask a question : ");
        in = new Scanner(System.in);
        String question = in.nextLine();

        String answer = assistant.chat(question);
        System.out.println(answer);
        System.out.println("-------------------------");

        boolean inChatMode = true;

        while (inChatMode) {
            System.out.println("> Another question about this topic ? Y or N");
            in = new Scanner(System.in);
            String continueChat = in.nextLine();
            switch (continueChat) {
                case "Y":
                    System.out.println("> Ask a question : ");
                    in = new Scanner(System.in);
                    question = in.nextLine();
                    answer = assistant.chat(question);
                    System.out.println(answer);
                    break;
                case "N": inChatMode = false;
                    break;
                default:
                    System.out.println("Unknown option, retry");
            }
        }
    }
}
