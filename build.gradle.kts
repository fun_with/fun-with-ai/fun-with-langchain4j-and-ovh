plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    val langchain4j_version = "0.30.0"
    implementation("dev.langchain4j:langchain4j:$langchain4j_version")
    implementation("dev.langchain4j:langchain4j-open-ai:$langchain4j_version")
    implementation("dev.langchain4j:langchain4j-embeddings:$langchain4j_version")
    implementation("dev.langchain4j:langchain4j-easy-rag:$langchain4j_version")
}

tasks.test {
    useJUnitPlatform()
}
